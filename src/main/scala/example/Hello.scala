package example


sealed trait Action[+M[_], +T]
case class Block[M[_],T](run : Unit => M[T]) extends Action[M, T]

object Action {
  def block[M[_], T](b : => M[T]) = Block(Unit => {b})

  def execute[M[_], T] (v : Action[M,T]) : M[T] =
    v match {
      case Block(run) => run()
    }
}


object Hello extends App {
  import Action._

  override def main(args: Array[String]): Unit = {
    println("Hello!")

    val v = block[Option, Int] (Some(100))

    val r = execute(v)

    println(s"result: $r")
  }
}

