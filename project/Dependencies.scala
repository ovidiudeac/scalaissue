import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"

  object cats {
    private val version = "1.0.1"
    val core = "org.typelevel" %% "cats-core" % version
  }
}
